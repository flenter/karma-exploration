describe("Example", function() {
    it("should have a message equal to 'Hello!'", function() {
        var example = {
            message: "Hello!"
        };
        expect(example.message).toBe('Hello!');
    });
});